#version 410 core
//uniform samplerCube tex_cubemap;

VS_OUT{
	vec3    tc;
} vs_out; 
uniform mat4 view;
uniform vec3 eye;
//layout (location = 0) out vec4 color; 

void main(void){
//    vec3[4] vertices = vec3[4](vec3(-1.0, -1.0, 1.0),
//                            vec3( 1.0, -1.0, 1.0),
//                            vec3(-1.0,  1.0, 1.0),
//                            vec3( 1.0,  1.0, 1.0));
    vec4[4] vertices = vec4[4](vec4(-1.0, -1.0, 1.0, 1.0),
                               vec4( 1.0, -1.0, 1.0, 1.0),
                               vec4(-1.0,  1.0, 1.0, 1.0),
                               vec4( 1.0,  1.0, 1.0, 1.0));
//    vs_out.tc =  mat3(view)*vertices[gl_VertexID] ;
    
    vec4 p = view * vertices[gl_VertexID];
    p /= p.w;
    vs_out.tc = normalize(p.xyz - eye);
    gl_Position = vertices[gl_VertexID];
//    gl_Position = vec4(vertices[gl_VertexID], 1.0);
}
