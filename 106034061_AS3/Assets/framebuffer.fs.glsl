#version 410
layout(location = 0) out vec4 fragColor;
//VertexData Structures
in VS_OUT
{
    vec2 texcoord;
	vec2 vsposition;
} fs_in;
//Shader and Settings
uniform sampler2D tex;
uniform int shader_now;
uniform vec2 img_size;
uniform vec2 iMouse;
uniform float time;
//Parameter
float afwidth;
float frequency;
float sigma_e = 2.0f;
float sigma_r = 2.8f;
float phi = 3.4f;
float tau = 0.99f;
float twoSigmaESquared = 2.0 * sigma_e * sigma_e;
float twoSigmaRSquared = 2.0 * sigma_r * sigma_r;
int halfWidth = int(ceil( 2.0 * sigma_r ));
int nbins = 8;
vec4 dog;
float PI=3.1415926;
vec3 texColor;
vec2 tc;
vec2 p;
float len;
vec2 uv;
vec3 col;
vec2 offset[9];
float aastep(float threshold, float value) {
    float uScale = 2.0, uYrot = 0.0;
#ifdef GL_OES_standard_derivatives
    float afwidth = 0.7 * length(vec2(dFdx(value), dFdy(value)));
#else
    float afwidth = frequency * (1.0 / 200.0) / uScale / cos(uYrot);
#endif
    return smoothstep(threshold - afwidth, threshold + afwidth, value);
}

void main()
{
    //Image Abstraction (Combine Blur + Quantization and DoG)///
    if (shader_now == 0){
        vec2 sum = vec2(0.0);
        vec2 norm = vec2(0.0);
        int kernel_count = 0;
        for ( int i = -halfWidth; i <= halfWidth; ++i ) {
            for ( int j = -halfWidth; j <= halfWidth; ++j ) {
                float d = length(vec2(i,j));
                vec2 kernel = vec2(exp( -d * d / twoSigmaESquared ),exp( -d * d / twoSigmaRSquared ));
                vec4 c = texture(tex, fs_in.texcoord + vec2(i,j) / img_size);
                vec2 L = vec2(0.299 * c.r + 0.587 * c.g + 0.114 * c.b);
                norm += 2.0 * kernel;
                sum += kernel * L;
            }
        }
        sum /= norm;
        float H = 100.0 * (sum.x - tau * sum.y);
        float edge = ( H > 0.0 )? 1.0 : 2.0 * smoothstep(-2.0, 2.0, phi * H );
        dog = vec4(edge,edge,edge,1.0 );
        vec4 blu = vec4(0);
        int n = 0;
        int half_size = 7;
        for ( int i = -half_size; i <= half_size; ++i ) {
            for ( int j = -half_size; j <= half_size; ++j ) {
                vec4 c = texture(tex, fs_in.texcoord + vec2(i,j)/img_size);
                blu+= c;
                n++;
            }
        }
        blu /=n;
        float r = floor(blu.r * float(nbins)) / float(nbins);
        float g = floor(blu.g * float(nbins)) / float(nbins);
        float b = floor(blu.b * float(nbins)) / float(nbins);
        vec4 quant = vec4(r,g,b,blu.a);
        if (dog.x + dog.y + dog.z > 0.8){
            fragColor = quant;
        }
        else{
            fragColor = dog;
        }
    }
    //Laplacian Filter///
    else if (shader_now == 1){
        int half_size = 1;
        float sum = 0;
        int n = 0;
        for ( int i = -half_size; i <= half_size; ++i ) {
            for ( int j = -half_size; j <= half_size; ++j ) {
                vec3 c = texture(tex, fs_in.texcoord + vec2(i,j)/img_size).rgb;
                float L = 0.299 * c.r + 0.587 * c.g + 0.114 * c.b;
                if(i==0 && j==0){
                    sum += 8 * L;
                }else{
                    sum -= L;
                }
                n++;
            }
        }
        sum /= n;
        // threshold
        if(sum >= 0.03){
            fragColor = vec4(1.0, 1.0, 1.0, 1.0);
        }else{
            fragColor = vec4(0.0, 0.0, 0.0, 1.0);
        }
    }
    //Pixelation
    else if (shader_now == 2){
        float Pixels = 512.0;
        float dx = 15.0 * (1.0 / Pixels);
        float dy = 10.0 * (1.0 / Pixels);
        vec2 Coord = vec2(dx * floor(fs_in.texcoord.x / dx), dy * floor(fs_in.texcoord.y / dy));
        fragColor = texture(tex, Coord);
    }
    //Sharpness Filter
    else if (shader_now == 3){
        int half_size = 1;
        vec3 sum = vec3(0.0);
        int n = 0;
        for ( int i = -half_size; i <= half_size; ++i ) {
            for ( int j = -half_size; j <= half_size; ++j ) {
                vec3 c = texture(tex, fs_in.texcoord + vec2(i,j)/img_size).rgb;
                if(i==0 && j==0){
                    sum += 9 * c;
                }else{
                    sum -= c;
                }
                n++;
            }
        }
        fragColor = vec4(sum, 1.0);
    }
    //Sine wave distortion
    else if (shader_now == 4){
        float power1 = 0.05;
        float power2 = 1;
        vec2 uv = vec2(fs_in.texcoord.x + power1*sin(fs_in.texcoord.y*power2*PI+time), fs_in.texcoord.y);
        if (uv.x >= 1.0)    uv.x = 1.0;
        vec3 texColor = texture(tex,uv).rgb;
        fragColor = vec4(texColor, 1.0);
    }
    //Red-Blue Stereo///
    else if (shader_now == 5)
    {
        vec4 texture_color_Left = texture(tex, fs_in.texcoord - vec2(0.005, 0.0));
        vec4 texture_color_Right = texture(tex, fs_in.texcoord + vec2(0.005, 0.0));
        vec4 texture_color = vec4(texture_color_Left.r * 0.29 + texture_color_Left.g * 0.58 + texture_color_Left.b * 0.114,texture_color_Right.g , texture_color_Right.b , 0.0);
        fragColor = texture_color;

    }
    //Bloom Effect
    else if (shader_now == 6)
    {
        int n =0;
        int half_size = 2;
        int nbins = 8;
        vec4 color = vec4(0);
        //blur
        for ( int i = half_size; i <= half_size ; ++i )
        {
            for (int j = half_size; j <= half_size ; ++j )
            {
                vec4 c= texture(tex,fs_in.texcoord + vec2(i,j)/img_size);
                color+= c;
                n++;
            }
        }
        color /=n;
        // Second blur
        half_size = 4;
        vec4 seccolor = vec4(0);
        for (int i = -half_size; i <= half_size ; ++i )
        {
            for (int j = half_size; j <= half_size ; ++j )
            {
                vec4 c= texture(tex,fs_in.texcoord + vec2(i,j)/img_size);
                seccolor+= c;
                n++;
            }
        }
        seccolor /=n;
        fragColor = texture(tex, fs_in.texcoord) * 0.6 + (color * 0.4 + seccolor * 0.2);
    }
    //Halftoning
    else if (shader_now == 7)
    {
//        vec2 p = fs_in.texcoord;
//        float pixelSize = 1.0 / float(600);
//        float dddx = mod(p.x, pixelSize) - pixelSize*0.5;
//        float dddy = mod(p.y, pixelSize) - pixelSize*0.5;
//        p.x -= dddx;
//        p.y -= dddy;
//        vec3 col = texture(tex, fs_in.texcoord).rgb;
//        float bright = 0.3333*(col.r+col.g+col.b);
//        float dist = sqrt(dddx*dddx + dddy*dddy);
//        float rad = bright * pixelSize * 0.8;
//        float m = step(dist, rad);
//        vec3 col2 = mix(vec3(0.0), vec3(1.0), m);
//        fragColor = vec4(col2, 1.0);

        float frequency = 120.0;
        vec2 st2 = mat2(0.707, -0.707, 0.707, 0.707) * fs_in.texcoord;
        vec2 nearest = 2.0 * fract(frequency * st2) - 1.0;
        float dist = length(nearest);
        // Use a texture to modulate the size of the dots
        vec3 texcolor = texture(tex, fs_in.texcoord).rgb;
        float radius = sqrt(1.0 - texcolor.g);
        vec3 white = vec3(1.0, 1.0, 1.0);
        vec3 black = vec3(0.0, 0.0, 0.0);
        vec3 fragcolor = mix(black, white, aastep(radius, dist));
        fragColor = vec4(fragcolor, 1.0);
    }
    // Cool shader 1 - Dream Vision
    else if (shader_now == 8)
    {
        vec2 uv = fs_in.texcoord.xy;
        vec4 c = texture(tex, uv);
        c += texture(tex, uv + 0.001);
        c += texture(tex, uv + 0.003);
        c += texture(tex, uv + 0.005);
        c += texture(tex, uv + 0.007);
        c += texture(tex, uv + 0.009);
        c += texture(tex, uv + 0.011);
        c += texture(tex, uv - 0.001);
        c += texture(tex, uv - 0.003);
        c += texture(tex, uv - 0.005);
        c += texture(tex, uv - 0.007);
        c += texture(tex, uv - 0.009);
        c += texture(tex, uv - 0.011);
        c.rgb = vec3((c.r + c.g + c.b) / 3.0);
        c = c / 10.5;
        fragColor = c;
    }
    // Cool shader-Predator's Thermal Vision
    else if (shader_now == 9)
    {
        vec2 uv = fs_in.texcoord.xy;
        vec3 c = vec3(1.0, 0.0, 0.0);
        vec3 pixcol = texture(tex, uv).rgb;
        vec3 colors[3];
        colors[0] = vec3(0., 0., 1.);
        colors[1] = vec3(1., 1., 0.);
        colors[2] = vec3(1., 0., 0.);
        float lum = (pixcol.r + pixcol.g + pixcol.b) / 3.;
        int ix = (lum < 0.5) ? 0 : 1;
        c = mix(colors[ix], colors[ix + 1], (lum - float(ix)*0.5) / 0.5);
        fragColor = vec4(c, 1.0);
    }
    //Normal
    else if(shader_now==10){
        vec3 texColor = texture(tex,fs_in.texcoord).rgb;
        fragColor = vec4(texColor, 1.0);
    }
    //Normal
    else{
        vec3 texColor = texture(tex,fs_in.texcoord).rgb;
        fragColor = vec4(texColor, 1.0);
    }
}
