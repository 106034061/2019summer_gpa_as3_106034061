#include "../Externals/Include/Include.h"

#define MENU_TIMER_START 11
#define MENU_TIMER_STOP 12
#define MENU_EXIT 13

#define MENU_IMAGE_ABSTRACTION 0
#define MENU_LAPLACIAN_FILTER 1
#define MENU_PIXELATION 2
#define MENU_SHARPNESS_FILTER 3
#define MENU_SINEWAVE_DISTORTION 4
#define MENU_RED_BLUE_STEREO 5
#define MENU_BLOOM_EFFECT 6
#define MENU_HALFTONING 7
#define MENU_DREAMVISION 8
#define MENU_PREDATOR 9
#define MENU_NORMAL 10
#define PI 3.1415926
using namespace glm;
using namespace std;

GLubyte timer_cnt = 0;
bool timer_enabled = true;
unsigned int timer_speed = 16;

int scene_seletion = 0;
float angle1 = 0.0, angle2 = 90.0;
float turn=0.0;
int now_position_x=0,now_position_y=0;

GLint um4p;
GLint um4mv;
GLint um4m;
GLint um4v;
GLint tex_location;
GLuint program;

mat4 view;                    // V of MVP, viewing matrix
mat4 projection;            // P of MVP, projection matrix
mat4 model= mat4(1.0f);                   // M of MVP, model matrix
mat4 offset_matrix1 = translate(mat4(), vec3(0.0f, -5.0f, 0.0f));
mat4 offset_matrix2 = translate(mat4(), vec3(0.0f, -50.0f, 0.0f));
//Camera
vec3 eye_position = vec3(0.0f, 0.0f, 0.0f);
vec3 front_back = vec3(1.0, 0.0f, 0.0f);//
vec3 up_down = vec3(0.0f, 1.0f, 0.0f);
vec3 right_left=vec3(0.0f,0.0f,1.0f);
vec3 right_left_rotate;
//Skybox
GLuint program_skybox;
GLuint skybox_vao;
GLuint skybox_tex;
GLint skybox_view;
GLint skybox_eye;
//Framebuffer
GLuint program_framebuffer;
GLuint window_vao;
GLuint window_buffer;
GLuint fbo;
GLuint fboDataTexture;
GLuint depthRBO;
int window_width, window_height;

static const GLfloat window_positions[] =
{
    1.0f,-1.0f,1.0f,0.0f,
    -1.0f,-1.0f,0.0f,0.0f,
    -1.0f,1.0f,0.0f,1.0f,
    1.0f,1.0f,1.0f,1.0f
};

static const GLenum draw_buffers[] =
{
    GL_COLOR_ATTACHMENT0,
    GL_COLOR_ATTACHMENT1
};
char** loadShaderSource(const char* file)
{
    FILE* fp = fopen(file, "rb");
    fseek(fp, 0, SEEK_END);
    long sz = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char *src = new char[sz + 1];
    fread(src, sizeof(char), sz, fp);
    src[sz] = '\0';
    char **srcp = new char*[1];
    srcp[0] = src;
    return srcp;
}

void freeShaderSource(char** srcp)
{
    delete[] srcp[0];
    delete[] srcp;
}
// define a simple data structure for storing texture image raw data
typedef struct _TextureData
{
    _TextureData(void) :
    width(0),
    height(0),
    data(0)
    {
    }
    int width;
    int height;
    unsigned char* data;
} TextureData;

// load a png image and return a TextureData structure with raw data
// not limited to png format. works with any image format that is RGBA-32bit
TextureData loadPNG(const char* const pngFilepath)
{
    TextureData texture;
    int components;
    
    // load the texture with stb image, force RGBA (4 components required)
    stbi_uc *data = stbi_load(pngFilepath, &texture.width, &texture.height, &components, 4);
    
    // is the image successfully loaded?
    if (data != NULL)
    {
        // copy the raw data
        size_t dataSize = texture.width * texture.height * 4 * sizeof(unsigned char);
        texture.data = new unsigned char[dataSize];
        memcpy(texture.data, data, dataSize);
        
        // mirror the image vertically to comply with OpenGL convention
        for (size_t i = 0; i < texture.width; ++i)
        {
            for (size_t j = 0; j < texture.height / 2; ++j)
            {
                for (size_t k = 0; k < 4; ++k)
                {
                    size_t coord1 = (j * texture.width + i) * 4 + k;
                    size_t coord2 = ((texture.height - j - 1) * texture.width + i) * 4 + k;
                    std::swap(texture.data[coord1], texture.data[coord2]);
                }
            }
        }
        // release the loaded image
        stbi_image_free(data);
    }
    return texture;
}

struct Material
{
    GLuint diffuse_tex;
};
struct Vertex{
    glm::vec3 Position;
    glm::vec2 TexCoords;
    glm::vec3 Normal;
};
struct Texture{
    GLuint id;
    string type;
    aiString path;
};
const char *skybox_vs_glsl[] =
{
    "#version 410 core                                              \n"
    "                                                               \n"
    "out VS_OUT                                                     \n"
    "{                                                              \n"
    "    vec3    tc;                                                \n"
    "} vs_out;                                                      \n"
    "                                                               \n"
    "uniform mat4 inv_vp_matrix;                                    \n"
    "uniform vec3 eye;                                              \n"
    "                                                               \n"
    "void main(void)                                                \n"
    "{                                                              \n"
    "    vec4[4] vertices = vec4[4](vec4(-1.0, -1.0, 1.0, 1.0),     \n"
    "                               vec4( 1.0, -1.0, 1.0, 1.0),     \n"
    "                               vec4(-1.0,  1.0, 1.0, 1.0),     \n"
    "                               vec4( 1.0,  1.0, 1.0, 1.0));    \n"
    "                                                               \n"
    "    vec4 p = inv_vp_matrix * vertices[gl_VertexID];            \n"
    "    p /= p.w;                                                  \n"
    "    vs_out.tc = normalize(p.xyz - eye);                        \n"
    "                                                               \n"
    "    gl_Position = vertices[gl_VertexID];                       \n"
    "}                                                              \n"
};

const char *skybox_fs_glsl[] =
{
    "#version 410 core                          \n"
    "                                           \n"
    "uniform samplerCube tex_cubemap;           \n"
    "                                           \n"
    "in VS_OUT                                  \n"
    "{                                          \n"
    "    vec3 tc;                                \n"
    "} fs_in;                                   \n"
    "                                           \n"
    "layout (location = 0) out vec4 color;      \n"
    "                                           \n"
    "void main(void)                            \n"
    "{                                          \n"
    "    color = texture(tex_cubemap, fs_in.tc);\n"
    "}                                          \n"
    "                                           \n"
};

class Mesh {
public:
    Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures, GLuint ID, int count){
        this->vertices = vertices;
        this->indices = indices;
        this->textures = textures;
        
        this->setupMesh();
        this->materialID=ID;
        this->drawCount=count;
    }
    void setupMesh(){
        glGenVertexArrays(1, &this->vao);
        glGenBuffers(1, &this->vbo);
        glGenBuffers(1, &this->ibo);
        
        glBindVertexArray(this->vao);
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
        glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);
        
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, TexCoords));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, Normal));
        glBindVertexArray(0);
    }
    // Render the mesh
    void Draw(){
        glBindVertexArray(this->vao);
        glBindTexture(GL_TEXTURE_2D, this->materialID);
        glDrawElements(GL_TRIANGLES, this->drawCount, GL_UNSIGNED_INT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
private:
    GLuint vao, ibo, vbo;
    vector<Vertex> vertices;
    vector<GLuint> indices;
    vector<Texture> textures;
    int drawCount;
    GLuint materialID;

};
class Model{
public:
    Model(){
    }
    void loadModel(const char * path){
        const aiScene *scene = aiImportFile(path, aiProcessPreset_TargetRealtime_MaxQuality);
        std::string path_str(path);
        this->directory = path_str.substr(0, path_str.find_last_of('/'));
        
        for (unsigned int i = 0; i < scene->mNumMaterials; ++i) {
            aiMaterial *source_material = scene->mMaterials[i];
            struct Material material;
            TextureData texture_data;
            aiString texturePath;
            if (source_material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == aiReturn_SUCCESS) {
                cout << "load texture image " << this->directory + "/" + (std::string)texturePath.C_Str() << endl;
                //texture_data[i] = loadPNG((char *)(this->directory + "/" + (std::string)texturePath.C_Str()).data());
                texture_data = loadPNG((char *)(this->directory + "/" + (std::string)texturePath.C_Str()).data());
            }
            else {
                cout << "load default image" << endl;
                //texture_data[i] = loadPNG((char *)"../Assets/default.png");
                texture_data = loadPNG((char *)"../Assets/default.png");
            }
            glGenTextures(1, &material.diffuse_tex);
            glBindTexture(GL_TEXTURE_2D, material.diffuse_tex);
            //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture_data[i].width, texture_data[i].height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data[i].data);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texture_data.width, texture_data.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data.data);
            glGenerateMipmap(GL_TEXTURE_2D);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            delete [] texture_data.data;
            // save material
            this->materials.push_back(material);
        }
        for (unsigned int i = 0; i < scene->mNumMaterials; ++i) {
            cout << this->materials[i].diffuse_tex << endl;
        }
        this->processNode(scene->mRootNode, scene);
    }
    
    void processNode(aiNode* node, const aiScene* scene){
        for (GLuint i = 0; i < node->mNumMeshes; i++){
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
            this->meshes.push_back(this->processMesh(mesh, scene));
        }
        for (GLuint i = 0; i < node->mNumChildren; i++){
            this->processNode(node->mChildren[i], scene);
        }
    }
    Mesh processMesh(aiMesh *mesh, const aiScene *scene){
        vector<Vertex> vertices;
        vector<GLuint> indices;
        vector<Texture> textures;
        for (GLuint i = 0; i < mesh->mNumVertices; i++){
            Vertex vertex;
            glm::vec3 vector;
            vector.x = mesh->mVertices[i].x;
            vector.y = mesh->mVertices[i].y;
            vector.z = mesh->mVertices[i].z;
            vertex.Position = vector;
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;
            
            if (mesh->mTextureCoords[0]) {
                glm::vec2 vec;
                vec.x = mesh->mTextureCoords[0][i].x;
                vec.y = mesh->mTextureCoords[0][i].y;
                vertex.TexCoords = vec;
            }
            else{
                vertex.TexCoords = glm::vec2(0.0f, 0.0f);
            }
            vertices.push_back(vertex);
        }
        for (GLuint i = 0; i < mesh->mNumFaces; i++){
            aiFace face = mesh->mFaces[i];
            for (GLuint j = 0; j < face.mNumIndices; j++){
                indices.push_back(face.mIndices[j]);
            }
        }
        // this->tex_map.push_back(this->mat[mesh->mMaterialIndex].diffuse_tex);
        return Mesh(vertices, indices, textures, this->materials[mesh->mMaterialIndex].diffuse_tex, mesh->mNumFaces*3);
    }
    void Draw(){
        for (GLuint i = 0; i < this->meshes.size(); i++){
            //glBindTexture(GL_TEXTURE_2D, this->mat[this->tex_map[i]].diffuse_tex);
            //glBindTexture(GL_TEXTURE_2D, this->tex_map[i]);
            this->meshes[i].Draw();
        }
    }
private:
    vector<Mesh> meshes;
    string directory;
    vector<struct Material> materials;
    vector<Texture> textures_loaded;
    vector<GLuint> tex_map;
};
Model model_1, model_2;
int sel = 0; // default is Normal
GLuint shader_status;
GLfloat iMouse[] = { 0.0f, 0.0f };
GLuint mouse_pos;
GLfloat img_size[] = { 600.0f, 600.0f };
GLuint img_resolution;
GLuint timestamp;

void skybox_Init(){
    program_skybox = glCreateProgram();
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vs, 1, skybox_vs_glsl, NULL);
    glShaderSource(fs, 1, skybox_fs_glsl, NULL);
    glCompileShader(vs);
    glCompileShader(fs);
    glAttachShader(program_skybox, vs);
    glAttachShader(program_skybox, fs);
    glLinkProgram(program_skybox);
    glUseProgram(program_skybox);
    skybox_view = glGetUniformLocation(program_skybox, "inv_vp_matrix");
    skybox_eye = glGetUniformLocation(program_skybox, "eye");
    TextureData front = loadPNG("../Assets/mp_hexagon/hexagon_ft.tga");
    TextureData back = loadPNG("../Assets/mp_hexagon/hexagon_bk.tga");
    TextureData left = loadPNG("../Assets/mp_hexagon/hexagon_lf.tga");
    TextureData right = loadPNG("../Assets/mp_hexagon/hexagon_rt.tga");
    TextureData up = loadPNG("../Assets/mp_hexagon/hexagon_up.tga");
    TextureData down = loadPNG("../Assets/mp_hexagon/hexagon_dn.tga");
    
    glGenTextures(1, &skybox_tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_tex);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 0, 0, GL_RGBA, front.width, front.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, front.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 1, 0, GL_RGBA, back.width, back.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, back.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 2, 0, GL_RGBA, up.width, up.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, up.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 3, 0, GL_RGBA, down.width, down.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, down.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 4, 0, GL_RGBA, left.width, left.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, left.data);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 5, 0, GL_RGBA, right.width, right.height , 0, GL_RGBA, GL_UNSIGNED_BYTE, right.data);
    
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glGenVertexArrays(1, &skybox_vao);
}
void model_Init(){
    program = glCreateProgram();
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    char** vertexShaderSource = loadShaderSource("vertex.vs.glsl");
    char** fragmentShaderSource = loadShaderSource("fragment.fs.glsl");
    glShaderSource(vertexShader, 1, vertexShaderSource, NULL);
    glShaderSource(fragmentShader, 1, fragmentShaderSource, NULL);
    freeShaderSource(vertexShaderSource);
    freeShaderSource(fragmentShaderSource);
    glCompileShader(vertexShader);
    glCompileShader(fragmentShader);
    shaderLog(vertexShader);
    shaderLog(fragmentShader);
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    um4p = glGetUniformLocation(program, "um4p");
    um4mv = glGetUniformLocation(program, "um4mv");
    tex_location = glGetUniformLocation(program, "tex");
    glUseProgram(program);
    
}
void framebuffer_Init() {
    program_framebuffer = glCreateProgram();
    GLuint vertexShader2 = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
    char** vertexShaderSource2 = loadShaderSource("framebuffer.vs.glsl");
    char** fragmentShaderSource2 = loadShaderSource("framebuffer.fs.glsl");
    glShaderSource(vertexShader2, 1, vertexShaderSource2, NULL);
    glShaderSource(fragmentShader2, 1, fragmentShaderSource2, NULL);
    freeShaderSource(vertexShaderSource2);
    freeShaderSource(fragmentShaderSource2);
    glCompileShader(vertexShader2);
    glCompileShader(fragmentShader2);
    shaderLog(vertexShader2);
    shaderLog(fragmentShader2);
    glAttachShader(program_framebuffer, vertexShader2);
    glAttachShader(program_framebuffer, fragmentShader2);
    glLinkProgram(program_framebuffer);
    glGenVertexArrays(1, &window_vao);
    glBindVertexArray(window_vao);
    glGenBuffers(1, &window_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, window_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(window_positions), window_positions, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * 4, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT) * 4, (const GLvoid*)(sizeof(GL_FLOAT) * 2));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glGenFramebuffers(1, &fbo);
    
    shader_status = glGetUniformLocation(program_framebuffer, "shader_now");
    mouse_pos = glGetUniformLocation(program_framebuffer, "iMouse");
    img_resolution = glGetUniformLocation(program_framebuffer, "img_size");
    timestamp = glGetUniformLocation(program_framebuffer, "time");
}

void My_Init()
{
    glClearColor(1.0f, 1.0f, 0.8f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
    GLint GlewInitResult = glewInit();
    // ---------------------------------------
    // skybox
    if (GLEW_OK != GlewInitResult)
    {
        printf("ERROR: %s",glewGetErrorString(GlewInitResult));
        exit(EXIT_FAILURE);
    }
    skybox_Init();
    // ---------------------------------------
    //Model
    if (GLEW_OK != GlewInitResult){
        printf("ERROR: %s",glewGetErrorString(GlewInitResult));
        exit(EXIT_FAILURE);
    }
    model_Init();
    model_1.loadModel("../Assets/sponza/sponza.obj");
    model_2.loadModel("../Assets/rungholt/house.obj");
    // ---------------------------------------
    //Framebuffer
    if (GLEW_OK != GlewInitResult){
        printf("ERROR: %s",glewGetErrorString(GlewInitResult));
        exit(EXIT_FAILURE);
    }
    framebuffer_Init();
}
void skybox_Display(){
    static const GLfloat gray[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    static const GLfloat ones[] = { 1.0f };
    vec3 eye =eye_position;
    mat4 inv_vp_matrix = inverse(projection * view);
    glClearBufferfv(GL_COLOR, 0, gray);
    glClearBufferfv(GL_DEPTH, 0, ones);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_tex);
    glUseProgram(program_skybox);
    glBindVertexArray(skybox_vao);
    glUniformMatrix4fv(skybox_view, 1, GL_FALSE, &inv_vp_matrix[0][0]);
    glUniform3fv(skybox_eye, 1, &eye[0]);
    glDisable(GL_DEPTH_TEST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glEnable(GL_DEPTH_TEST);
}
void model_Display(){
    glUseProgram(program);
    glUniformMatrix4fv(um4mv, 1, GL_FALSE, value_ptr(view * model));
    glUniformMatrix4fv(um4m, 1, GL_FALSE, value_ptr(model));
    glUniformMatrix4fv(um4v, 1, GL_FALSE, value_ptr(view));
    glUniformMatrix4fv(um4p, 1, GL_FALSE, value_ptr(projection));
    glUniform1i(tex_location, 0);
}
void framebuffer_Display(){
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glBindVertexArray(window_vao);
    glUseProgram(program_framebuffer);
    glUniform1i(shader_status, sel);
    glUniform2fv(mouse_pos, 1, iMouse);
    glUniform2fv(img_resolution, 1, img_size);
    glUniform1f(timestamp, glutGet(GLUT_ELAPSED_TIME) / 1000.0);
    glActiveTexture(GL_TEXTURE0);
    
    glBindTexture(GL_TEXTURE_2D, fboDataTexture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
void My_Display()
{
    // Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
    // Clear the framebuffer with red
    static const GLfloat red[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    //static const GLfloat white[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    static const GLfloat one = { 1.0f };
    glClearBufferfv(GL_COLOR, 0, red);
    glClearBufferfv(GL_DEPTH, 0, &one);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    view = lookAt(eye_position, eye_position + front_back,up_down );
    // --------------------------------------------------------------
    skybox_Display();
    // --------------------------------------------------------------
    model_Display();
    // --------------------------------------------------------------
    if (scene_seletion == 0){
        model = offset_matrix1;
        model_1.Draw();
    }
    else{
        model = offset_matrix2;
        model_2.Draw();
    }
    // --------------------------------------------------------------
    framebuffer_Display();
    // --------------------------------------------------------------
    glutSwapBuffers();
}
void framebuffer_Reshape(int width, int height){
    glDeleteRenderbuffers(1, &depthRBO);
    glDeleteTextures(1, &fboDataTexture);
    glGenRenderbuffers(1, &depthRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);
    glGenTextures(1, &fboDataTexture);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture, 0);
}
void My_Reshape(int width, int height){
    glViewport(0, 0, width, height);
    float viewportAspect = (float)width / (float)height;
    projection = perspective(radians(60.0f), viewportAspect, 0.1f, 1000.0f);
    view = lookAt(vec3(7.5f, 5.0f, 7.5f), vec3(0.0f, 1.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
    framebuffer_Reshape(width, height);
}

void My_Timer(int val)
{
    timer_cnt++;
    glutPostRedisplay();
    if(timer_enabled)
    {
        glutTimerFunc(timer_speed, My_Timer, val);
    }
}

void My_Mouse(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON)
    {
        if (state == GLUT_DOWN)
        {
            printf("Mouse %d is pressed at (%d, %d)\n", button, x, y);
            now_position_x=x;
            now_position_y=y;
        }
        else if (state == GLUT_UP)
        {
            printf("Mouse %d is released at (%d, %d)\n", button, x, y);
        }
    }
    iMouse[0] = x, iMouse[1] = y;

}
void Eye_Direction(int dx, int dy) {
    printf("moving (%d, %d)\n", dx, dy);
    //polar coordinate
    angle1 -= (float)dx/10;//horizontal-axis
    angle2 -= (float)dy/10;//vertical-axis
    if (angle1 > 360.0)
        angle1 -= 360.0;
    else if (angle1 < 0)
        angle1 += 360;
    
//    if (angle2 > 180.0)
//        angle2 =179;
//    else if (angle2 < 0)
//        angle2 =1;
    
    front_back.x = sinf(angle2 / 180.0 * PI) * std::cosf(angle1/ 180.0 * PI);
    front_back.z = sinf(angle2 / 180.0 * PI) * std::sinf(angle1 / 180.0 * PI);
    front_back.y = cosf(angle2 / 180.0 * PI);
    
    right_left_rotate = cross(front_back, up_down);
}
void My_Keyboard(unsigned char key, int x, int y){
    printf("Key %c is pressed at (%d, %d)\n", key, x, y);
    
    switch (key){
        case 's':
            eye_position -= front_back;
            /////////////////////
//            eye_position.x-=front_back.x;
//            eye_position.z-=front_back.z;
            break;
        case 'w':
            eye_position += front_back;
            /////////////////////
//            eye_position.x+=front_back.x;
//            eye_position.z+=front_back.z;
            break;
        case 'a':
//            eye_position -= right_left_rotate;
            ///////////////////////
//            turn+=0.01f;
//            front_back.x=sin(turn);
//            front_back.z=cos(turn);
            eye_position-=right_left;
            break;
        case 'd':
//            eye_position += right_left_rotate;
            ///////////////////////
//            turn-=0.01f;
//            front_back.x=sin(turn);
//            front_back.z=cos(turn);
            eye_position+=right_left;
            break;
        case 'z':
            eye_position += up_down;
            break;
        case 'x':
            eye_position -= up_down;
            break;
        case 'c':
            if (scene_seletion == 0)
                scene_seletion= 1;
            else
                scene_seletion = 0;
            break;
        case 'e':
            sel = (sel + 1) % 11;
            cout << "sel =" << sel << endl;
            break;
        case 'q':
            if(sel==0)sel=9;
            else sel-=1;
            cout << "sel =" << sel << endl;
            break;

    }
}

void My_SpecialKeys(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            printf("F1 is pressed at (%d, %d)\n", x, y);
            break;
        case GLUT_KEY_PAGE_UP:
            printf("Page up is pressed at (%d, %d)\n", x, y);
            break;
        case GLUT_KEY_LEFT:
            printf("Left arrow is pressed at (%d, %d)\n", x, y);
            break;
        case GLUT_KEY_RIGHT:
            printf("Right arrow is pressed at (%d, %d)\n", x, y);
            break;
        case GLUT_KEY_UP:
            printf("Up arrow is pressed at (%d, %d)\n", x, y);
            break;
        case GLUT_KEY_DOWN:
            printf("Down arrow is pressed at (%d, %d)\n", x, y);
            break;
        default:
            printf("Other special key is pressed at (%d, %d)\n", x, y);
            break;
    }
    //    glutPostRedisplay();
}
void My_Menu(int id)
{
	switch(id)
	{
	case MENU_TIMER_START:
		if(!timer_enabled)
		{
			timer_enabled = true;
			glutTimerFunc(timer_speed, My_Timer, 0);
		}
		break;
	case MENU_TIMER_STOP:
		timer_enabled = false;
		break;
	case MENU_EXIT:
		exit(0);
		break;
    case MENU_IMAGE_ABSTRACTION:
        sel = 0;
        break;
    case MENU_LAPLACIAN_FILTER:
        sel = 1;
        break;
    case MENU_PIXELATION:
        sel = 2;
        break;
    case MENU_SHARPNESS_FILTER:
        sel = 3;
        break;
    case MENU_SINEWAVE_DISTORTION:
        sel = 4;
        break;
    case MENU_RED_BLUE_STEREO:
        sel = 5;
        break;
    case MENU_BLOOM_EFFECT:
        sel = 6;
        break;
    case MENU_HALFTONING:
        sel = 7;
        break;
    case MENU_DREAMVISION:
        sel = 8;
        break;
    case MENU_PREDATOR:
        sel = 9;
        break;
    case MENU_NORMAL:
        sel = 10;
        break;
	default:
		break;
	}
}
void Move_Mouse(int x,int y){
    cout<<"Mouse Moving:x->"<<x<<", y->"<<y<<endl;
    Eye_Direction(x-now_position_x, y-now_position_y);
    
    now_position_x=x;
    now_position_y=y;
}
int main(int argc, char *argv[])
{
#ifdef __APPLE__
    // Change working directory to source code path
    chdir(__FILEPATH__("/../Assets/"));
#endif
	////////////////////
	glutInit(&argc, argv);
#ifdef _MSC_VER
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#else
    glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1200, 900);
	glutCreateWindow("106034061_AS3");
#ifdef _MSC_VER
	glewInit();
#endif
    glPrintContextInfo();
	My_Init();

	int menu_main = glutCreateMenu(My_Menu);
    int menu_timer = glutCreateMenu(My_Menu);

	glutSetMenu(menu_main);
	glutAddSubMenu("Timer", menu_timer);
    
    glutAddMenuEntry("Image Abstraction", MENU_IMAGE_ABSTRACTION);
    glutAddMenuEntry("Laplacian Filter", MENU_LAPLACIAN_FILTER);
    glutAddMenuEntry("Sharpeness Filter", MENU_SHARPNESS_FILTER);
    glutAddMenuEntry("Pixelization", MENU_PIXELATION);
    glutAddMenuEntry("Red-Blue Stereo", MENU_RED_BLUE_STEREO);
    glutAddMenuEntry("Sine Wave Distortion", MENU_SINEWAVE_DISTORTION);
    glutAddMenuEntry("Bloom Effect", MENU_BLOOM_EFFECT);
    glutAddMenuEntry("Halftoning", MENU_HALFTONING);
    glutAddMenuEntry("Dream Vision", MENU_DREAMVISION);
    glutAddMenuEntry("Predator's Thermal Vision", MENU_PREDATOR);
    glutAddMenuEntry("Normal", MENU_NORMAL);

	glutAddMenuEntry("Exit", MENU_EXIT);

	glutSetMenu(menu_timer);
	glutAddMenuEntry("Start", MENU_TIMER_START);
	glutAddMenuEntry("Stop", MENU_TIMER_STOP);

	glutSetMenu(menu_main);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	glutDisplayFunc(My_Display);
	glutReshapeFunc(My_Reshape);
	glutMouseFunc(My_Mouse);
	glutKeyboardFunc(My_Keyboard);
	glutSpecialFunc(My_SpecialKeys);
	glutTimerFunc(timer_speed, My_Timer, 0); 
    glutMotionFunc(Move_Mouse);

    glutMainLoop();
	return 0;
}
